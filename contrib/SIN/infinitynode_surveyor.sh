#!/bin/bash
# Copyright (c) 2019 The SIN Core developers
# Auth: xtdevcoin
#
# this script control the status of node
# 1. node is active
# 2. infinitynode is ENABLED
# 3. infinitynode is not ENABLED
# 4. node is stopped by supplier - maintenance
# 5. node is frozen - dead lock
#
# Add in crontab when YOUR NODE HAS STATUS ENABLED:
# */5 * * * * /full_path_to/infinitynode_surveyor.sh
#
# change path of "tny_deamon" and "tny_cli"
#
# TODO: 1. upload status of node to server for survey
#       2. chech status of node from explorer
#

tny_deamon_name="tnyd"

## PLEASE CHANGE THIS
tny_deamon="/usr/local/bin/tnyd"
tny_cli="/usr/local/bin/tny-cli"
##

DATE_WITH_TIME=`date "+%Y%m%d-%H:%M:%S"`

function start_node() {
	echo "$DATE_WITH_TIME : delete caches files debug.log db.log fee_estimates.dat governance.dat mempool.dat mncache.dat mnpayments.dat netfulfilled.dat" >> ~/.sin/tny_control.log 
	cd ~/.sin && rm debug.log db.log fee_estimates.dat governance.dat mempool.dat mncache.dat mnpayments.dat netfulfilled.dat
	sleep 5
	echo "$DATE_WITH_TIME : Start sin deamon $tny_deamon" >> ~/.sin/tny_control.log
	echo "$DATE_WITH_TIME : tny_START" >> ~/.sin/tny_control.log
	$tny_deamon &
}

function stop_start_node() {
	echo "$DATE_WITH_TIME : delete caches files debug.log db.log fee_estimates.dat governance.dat mempool.dat mncache.dat mnpayments.dat netfulfilled.dat" >> ~/.sin/tny_control.log 
	cd ~/.sin && rm debug.log db.log fee_estimates.dat governance.dat mempool.dat mncache.dat mnpayments.dat netfulfilled.dat
	echo "$DATE_WITH_TIME : kill process by name $tny_deamon_name" >> ~/.sin/tny_control.log
	echo "$DATE_WITH_TIME : tny_STOP" >> ~/.sin/tny_control.log
	pgrep -f $tny_deamon_name | awk '{print "kill -9 " $1}' | sh >> ~/.sin/tny_control.log
	sleep 15
	echo "$DATE_WITH_TIME : Restart sin deamon $tny_deamon" >> ~/.sin/tny_control.log
	echo "$DATE_WITH_TIME : tny_START" >> ~/.sin/tny_control.log
	$tny_deamon &
}

timeout --preserve-status 10 $tny_cli getblockcount
CHECK_tny=$?
echo "$DATE_WITH_TIME : check status of tnyd: $CHECK_tny" >> ~/.sin/tny_control.log

#node is active
if [ "$CHECK_tny" -eq "0" ]; then
	echo "$DATE_WITH_TIME : sin deamon is active" >> ~/.sin/tny_control.log
	SINSTATUS=`$tny_cli masternode status | grep "successfully" | wc -l`

	#infinitynode is ENABLED
	if [ "$tnySTATUS" -eq "1" ]; then
		echo "$DATE_WITH_TIME : infinitynode is started." >> ~/.sin/tny_control.log
	else
		echo "$DATE_WITH_TIME : node is synchronising...please wait!" >> ~/.sin/tny_control.log
	fi
fi

#node is stopped by supplier - maintenance
if [ "$CHECK_tny" -eq "1" ]; then
	#find tnyd
	SIND=`ps -e | grep $tny_deamon_name | wc -l`
	if [ "$tnyD" -eq "0" ]; then
		start_node
	else
		stop_start_node
	fi
fi

#command not found
if [ "$CHECK_tny" -eq "127" ]; then
	echo "$DATE_WITH_TIME : Command not found. Please change the path of tny_deamon and tny_cli." >> ~/.sin/tny_control.log
fi

#node is frozen
if [ "$CHECK_tny" -eq "143" ]; then
	echo "$DATE_WITH_TIME : sin deamon will be restarted...." >> ~/.sin/tny_control.log
	stop_start_node
fi
